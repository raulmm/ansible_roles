ldap_vault
=========

Contains the vault with the details to connect to AD for user authentication.

Requirements
------------

No requirements

Role Variables
--------------

INPUT: none
OUTPUT:
  vault_ldap_user:    AD user
  vault_ldap_pwd:     AD user password

Dependencies
------------

None

Example Playbook
----------------

    - hosts: tower
      roles:
         - ldap_vault
         - configure_central_autentication

License
-------

GPLv3

Author Information
------------------

Raul Mahiques <rmahique@redhat.com>
