# BEGIN 99-ethtool.sh

# This file must be made executable!
#
# If an ifcfg file has ETHTOOL_OPTS set
# this dispatcher will invoke the ethtool_set
# function from the legacy network service
# scripts.
#
# Patrick Talbert <ptalbert@redhat.com>
#

if [[ $2 == up ]]
then
    SCRIPT="$(basename "$0")"
    if [[ -e $CONNECTION_FILENAME ]]
    then
        source /etc/sysconfig/network-scripts/network-functions
        source $CONNECTION_FILENAME
        if [[ -n $ETHTOOL_OPTS ]]
        then
            logger "$SCRIPT: Calling ethtool_set for $1..."
            ethtool_set
        else
            logger "$SCRIPT: ETHTOOL_OPTS not in $CONNECTION_FILENAME, skipping"
        fi
    else
        logger "$SCRIPT: $CONNECTION_FILENAME does not exist?"
    fi
fi

# END 99-ethtool.sh
