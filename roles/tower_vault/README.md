tower_vault
=========

Contains the vault for ansible tower

Requirements
------------

No requirements

Role Variables
--------------

INPUT: none
OUTPUT:
  vault_awx_user:    Tower admin user
  vault_awx_pwd:     Tower admin user password
  vault_awx_license  Tower license

Dependencies
------------

None

Example Playbook
----------------

    - hosts: tower
      roles:
         - tower_prod_vault
         - do_things_on_tower

License
-------

GPLv3

Author Information
------------------

Raul Mahiques <rmahique@redhat.com>
