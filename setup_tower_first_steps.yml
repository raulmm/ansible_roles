---
# This will setup tower for the first time and trigger all the necessary steps
# Requisites: 
#  - This file and vaults.yml
#  - Tower installed (and the inventory file used to install it.)
#  - Firewall rules created
# 
# Instructions:
# We place this and vaults.yml file inside /root directory of one of the tower servers from which setup.sh was run.
# We create a temporary file. ie. .pwd2 in the same directory containing the passwor for vaults.yml vault.
# Run the playbook
#  ansible-playbook --vault-password-file=.pwd2 -i ansible-tower.../inventory setup_tower_first_steps.yml
#
# Notes:
# If you want to run specific parts only you can use -t tag_name, to see the list of tags in the playbook run:
# grep -i tags: setup_tower_first_steps.yml
#
- name: Setup first tower project that include the inital playbook and templates, etc..
  hosts: localhost
  gather_facts: no
  connection: local
  become: no
  vars:
    awx_hostname: "your_tower_hostname"
    awx_user: "{{ vault_awx_user }}"  # Edit vaults.yml, password is in .vault_pwd
    awx_pwd: "{{ vault_awx_pwd }}"    # Edit vaults.yml, password is in .vault_pwd
    awx_project_settings:
      name: "Playbooks GIT"
      description: "GIT Repository with playbooks"
      scm_type: "git"
      scm_url: "https://gitlab.com/raulmm/ansible_roles"
      scm_branch: "master"
      scm_clean: false
      scm_delete_on_update: false
      credential: null
      timeout: 120
      organization: 1
      scm_update_on_launch: true
      scm_update_cache_timeout: 0
    awx_inventory_localhost:
      name: "Localhost"
      description: "Contains localhost only"
      organization: 1
      id: 1
    awx_inventory_tower:
      name: "Tower"
      description: "Tower infrastructure"
      organization: 1
      id: 2
      variables: '---\nansible_ssh_extra_args: \"-o StrictHostKeyChecking=false\"\nhost_key_checking: false\nremote_user: ansible'
    awx_template_configureTower:
      name: "Configure tower"
      description: "Configures tower according to the settings defined"
      job_type: run
      playbook: "configure_tower.yml"
      forks: 0
      limit: localhost, tower
      verbosity: 0
      extra_vars: ""
      job_tags: ""
      force_handlers: false
      skip_tags: ""
      start_at_task: ""
      timeout: 0
      use_fact_cache: false
      host_config_key: ""
      ask_diff_mode_on_launch: false
      ask_variables_on_launch: false
      ask_limit_on_launch: false
      ask_tags_on_launch: false
      ask_skip_tags_on_launch: false
      ask_job_type_on_launch: false
      ask_verbosity_on_launch: false
      ask_inventory_on_launch: false
      ask_credential_on_launch: false
      survey_enabled: false
      become_enabled: false
      diff_mode: false
      allow_simultaneous: false
      custom_virtualenv: null
      job_slice_count: 1
      credential: null
#      vault_credential: null
  vars_files:
    - vaults.yml
  pre_tasks:
    - name: Add localhost to the inventory
      add_host:
        hostname: localhost
        groups: all
      tags: always
    - name: Print inventory
      debug: var=groups
      tags: always
  tasks:
    - name: Create the first project
      block:
      - name: Setup the first project
        uri:
          url: "https://{{ awx_hostname }}/api/v2/projects/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: POST
          body_format: json
          body: "{{ awx_project_settings | to_nice_json}}"
          force_basic_auth: yes
          status_code:
            - 200
            - 201
        register: my_first_project
        tags: project

      - name: Debug the first project
        debug: var=my_first_project
        tags: project
      when: inventory_hostname == "localhost"

    - name: Create vaults
      block:
      - name: Create the vaults
        uri:
          url: "https://{{ awx_hostname }}/api/v2/credentials/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: POST
          body_format: json
          body: "{{ item | to_nice_json}}"
          force_basic_auth: yes
          status_code:
            - 200
            - 201
        loop: "{{ vaults }}"
        register: my_vaults
        no_log: true

      - name: Create variable with vault ids
        set_fact:
          svaults: "{{ svaults | default({})| combine ( {item['json']['name']: item['json']['id'] } , recursive=False) }}"
        loop: "{{ my_vaults['results'] }}"
        no_log: true
      rescue:
      - name: Try to remove the vaults if something failed
        uri:
          url: "https://{{ awx_hostname }}/api/v2/credentials/{{ item['json']['id'] }}/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: DELETE
          force_basic_auth: yes
          status_code:
            - 204
        loop: "{{ my_vaults['results'] | flatten(levels=1) }}"
        no_log: true
      when: inventory_hostname == "localhost"
      tags: vaults

    - name: Create inventories
      block:
      - name: Turn demo inventory into localhost inventory
        uri:
          url: "https://{{ awx_hostname }}/api/v2/inventories/{{ awx_inventory_localhost['id'] }}/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: PATCH
          body_format: json
          body: "{{ awx_inventory_localhost | to_nice_json}}"
          force_basic_auth: yes
          status_code:
            - 200
            - 201

      - name: Debug inventory
        debug: var=my_inventory['json']['id']

      - name: Create a tower inventory (via API)
        uri:
          url: "https://{{ awx_hostname }}/api/v2/inventories/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: POST
          body_format: json
          body: "{{ awx_inventory_tower | to_nice_json}}"
          force_basic_auth: yes
          status_code:
            - 200
            - 201
        register: my_towerinv

      - name: Populate the tower inventory with a single group
        uri:
          url: "https://{{ awx_hostname }}/api/v2/inventories/{{ my_towerinv['json']['id'] }}/groups/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: POST
          body_format: json
          body: "{ \"name\": \"tower\", \"description\": \"tower\" }"
          force_basic_auth: yes
          status_code:
            - 200
            - 201
        register: my_igroup

      # TO-DO: Here we asume you are running this playbook using the inventory file that is used for tower installation, a better way would be to retrieve the list of tower instances from the API.
      - name: Populate the tower inventory with the tower hosts
        uri:
          url: "https://{{ awx_hostname }}/api/v2/groups/{{ my_igroup['json']['id'] }}/hosts/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: POST
          body_format: json
          body: "{ \"name\": \"{{ item }}\", \"description\": \"{{ item }}\",  \"inventory\": \"{{ my_towerinv['json']['id'] }}\", \"enabled\": \"true\" }"
          force_basic_auth: yes
          status_code:
            - 200
            - 201
        register: my_thosts
        loop: "{{ groups['tower'] }}"
      rescue:
      - name: Remove The inventory if something fails
        uri:
          url: "https://{{ awx_hostname }}/api/v2/inventories/{{ my_towerinv['json']['id'] }}/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: DELETE
          force_basic_auth: yes
          status_code:
            - 200
            - 202
      when: inventory_hostname == "localhost"
      tags: inventory

    - name: Create the template
      block:
      - name: Populate the variable for the template with the rest of details
        set_fact:
          awx_template_configureTower: "{{ awx_template_configureTower | combine({ item[0]: item[1] })  }}"
        loop:
          - "[ 'inventory', {{ my_towerinv['json']['id'] }} ]"
          - "[ 'project', {{ my_first_project['json']['id'] }} ]"

      - name: print awx_template_configureTower variable
        debug: var=awx_template_configureTower

      - name: print awx_template_configureTower variable
        debug: var=item
        loop:
          - "[ 'inventory', {{ my_towerinv['json']['id'] }} ]"
          - "[ 'project', {{ my_first_project['json']['id'] }} ]"

      - name: Schedule a project update
        uri:
          url: "https://{{ awx_hostname }}/api/v2/projects/9/update/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: POST
          force_basic_auth: yes
          status_code:
            - 200
            - 201
            - 202
        register: my_first_project

      # TO-DO: a better way tould be to query the API instead of using this simple timeout.
      - name: Wait until the project has been updated
        wait_for:
          timeout: 15

      - name: create teamplate to configure tower
        uri:
          url: "https://{{ awx_hostname }}/api/v2/job_templates/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: POST
          body_format: json
          body: "{{ awx_template_configureTower | to_nice_json}}"
          force_basic_auth: yes
          status_code:
            - 200
            - 201
        register: my_template

      - name: Debug template
        debug: var=my_template

      - name: Add credentials to the new template
        uri:
          url: "https://{{ awx_hostname }}/api/v2/job_templates/{{ my_template['json']['id'] }}/credentials/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: POST
          body_format: json
          body: "{ \"id\": {{ item[0] }} }"
          force_basic_auth: yes
          status_code:
            - 200
            - 204
        register: my_cred_template
        loop:
          - [ 91 ]
          - [ 92 ]
      rescue:
      - name: Remove the template if somethign failed
        uri:
          url: "https://{{ awx_hostname }}/api/v2/job_templates/{{ my_template['json']['id'] }}/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: DELETE
          force_basic_auth: yes
          status_code:
            - 200
            - 204
      when: inventory_hostname == "localhost"
      tags: template

    - name: Execute the template
      block:
      - name: Find the template id
        uri:
          url: "https://{{ awx_hostname }}/api/v2/job_templates/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: GET
          force_basic_auth: yes
          status_code:
            - 200
        register: my_templates
        when: my_template is undefined

      - name: Debug inventory
        debug: var=my_templates['json']['results']
        when: my_template is undefined

      - name: Get the right template ID
        set_fact:
          template_id: "{{ item['id'] }}"
        loop: "{{ my_templates['json']['results'] }}"
        when: my_template is undefined and item['name'] == awx_template_configureTower['name']

      - name: Set the template ID from the recently added template
        set_fact:
          template_id: "{{ my_template['json']['id'] }}"
        when: my_template is defined

      - name: Trigger the template run
        uri:
          url: "https://{{ awx_hostname }}/api/v2/job_templates/{{ template_id }}/launch/"
          user: "{{ awx_user }}"
          password: "{{ awx_pwd }}"
          method: POST
          force_basic_auth: yes
          status_code:
            - 200
            - 201
        register: my_jobrun
      tags: jobrun
      when: inventory_hostname == "localhost"
...
